'use strict';

const express      = require('express');
const bodyParser   = require('body-parser');
const app          = express();
const Web3	   = require('web3')
var greeter = require('./greeter.json')
var economy = require('./Economy.json')


var provider = new Web3.providers.HttpProvider('http://107.23.87.210:8545')
//var provider = new Web3.providers.HttpProvider('http:/66.207.221.230:7545')
var web3 = new Web3(provider)

const contract = require('truffle-contract')


app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
bodyParser.urlencoded({ extended: true })
  next();
});
app.get('/accounts', (req, res) => {

        const economy_contract = contract(economy)

        economy_contract.setProvider(web3.currentProvider)
        web3.eth.getAccounts((error, accounts) => {
                economy_contract.deployed().then((instance) => {
                        instance.getNumMembers().then((response) => {
				let numMembersPromiseArray = []
                                for (var i=0; i<response; i++) {
					numMembersPromiseArray.push(instance._getMemberProfile(i))
				}
				Promise.all(numMembersPromiseArray).then(results => {
					var obj = []
					var i = 0
					results.forEach(r => {
						
						obj.push({ 
							member: r[0],
							name: r[1],
							score: r[2],
							age: r[3],
							sex: r[4],
							imageURL: r[5],
							location: r[6],
							idx: i
						})
						i++
					})
					res.send(obj)

				})
                                //res.send({ score: response[2]  })
                        })

                })
        })



});

app.get('/score/:id', (req, res) => {

	const economy_contract = contract(economy)

	economy_contract.setProvider(web3.currentProvider)
	web3.eth.getAccounts((error, accounts) => {
		economy_contract.deployed().then((instance) => {
			instance.getMemberScore(req.params.id).then((response) => {
				console.log(response)
				res.send({ score: response[2]  })
			})

		})
	})
	
});

app.get('/account/:id', (req, res) => {

        const economy_contract = contract(economy)

        economy_contract.setProvider(web3.currentProvider)
        web3.eth.getAccounts((error, accounts) => {
                economy_contract.deployed().then((instance) => {
                        instance._getMemberProfile(req.params.id).then((r) => {
                        	res.send({
                                	member: r[0],
                                        name: r[1],
                                        score: r[2],
                                        age: r[3],
                                      	sex: r[4],
                                        imageURL: r[5],
                                        balance: r[6],
                                        idx: req.params.id
				})
                                
                        })

                })
        })

});


app.get('/vote/:address/up', (req, res) => {

        const economy_contract = contract(economy)

        economy_contract.setProvider(web3.currentProvider)
        web3.eth.getAccounts((error, accounts) => {
                economy_contract.deployed().then((instance) => {
                        console.log(accounts[1])
                        console.log(accounts[0])
                        instance._vote(true,accounts[1], { from: accounts[0], gas: 4000000 }).then((response) => {
                                console.log(response)
                                res.send({ score: response  })
                        })

                })
        })

});


app.get('/vote/:address/down', (req, res) => {

        const economy_contract = contract(economy)

        economy_contract.setProvider(web3.currentProvider)
        web3.eth.getAccounts((error, accounts) => {
                economy_contract.deployed().then((instance) => {
			console.log(accounts[1])
			console.log(accounts[0])
                        instance._vote(false,accounts[1], { from: accounts[0], gas: 4000000 }).then((response) => {
                                console.log(response)
                                res.send({ score: response  })
                        })

                })
        })

});

app.get('/distribute', (req, res) => {

        const economy_contract = contract(economy)

        economy_contract.setProvider(web3.currentProvider)
        web3.eth.getAccounts((error, accounts) => {
                economy_contract.deployed().then((instance) => {
                        instance.distributeWealth({ from: accounts[0], gas: 4000000 }).then((response) => {
                                console.log(response)
                                res.send({ status: "distributed"  })
                        })

                })
        })

});





app.listen(9000, () => {
        console.log('Server started on port ' + '9000');
});
